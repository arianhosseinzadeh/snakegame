var _ = require('lodash');
var User = require('./User');
var Common = require('../common/common');
var Game = require('./game');
module.exports = function() {
    var Users = {};
    var Inviters = {};
    var Inviteds = {};
    var UserNames = {};
    var Games = {}; // gameId to game
    var userNameExists = function(userName) {
        return _.has(UserNames, userName);
    };
    var getUserById = function(id) {
        return Users[id];
    };
    var deleteGame = function(gameId) {
        var game = Games[gameId];
        game.getPlayers().map(function(playerId) {
            Users[playerId].free();
            Users[playerId].socket.leave(gameId);
        });
        game.getWatchers().map(function(watcherId) {
            Users[watcherId].free();
            Users[watcherId].socket.leave(gameId);
        });
        delete Games[gameId];
    };
    var getLimitedUsersList = function() {//{freeUsers: {},matches: {gameId:{players: [],numberOfWatchers: number}}}
        var result = {
            freeUsers: {},
            matches: {}
        };
        for (var usr in Users) {
            if (Users.hasOwnProperty(usr) && Users[usr].isFree() || Users[usr].isWatching()) {
                result.freeUsers[usr] = Users[usr].userName;
            }
        }
        for (var gameId in Games) {
            if (Games.hasOwnProperty(gameId)) {
                result.matches[gameId] = {};
                result.matches[gameId].players = [];
                result.matches[gameId].players.push(Users[Games[gameId].getPlayers()[0]].userName);
                result.matches[gameId].players.push(Users[Games[gameId].getPlayers()[1]].userName);
                result.matches[gameId].numberOfWatchers = Games[gameId].getNumberOfWatchers();
            }
        }
        return result;
    };
    var addInvitation = function(inviterId, invitedId) {
        if (!_.has(Inviters, inviterId)) {
            Inviters[inviterId] = [];
        }
        Inviters[inviterId].push(invitedId);
        if (!_.has(Inviteds, invitedId)) {
            Inviteds[invitedId] = [];
        }
        Inviteds[invitedId].push(inviterId);
    };
    var deleteInvitation = function(inviter, invited) {
        delete Inviters[inviter];
        delete Inviteds[invited];
    };
    var invitationExists = function(inviter, invited) {
        return Common.arrayContains(Inviters[inviter], invited);
    };
    var getUserNameById = function(userId) {
        return Users[userId].userName;
    };
    var deleteUser = function(userId) {
        var user = Users[userId];
        leaveGame(userId);
        if (_.has(Inviteds, userId)) {
            Inviteds[userId].map(function(inviter) {
                Inviters[inviter] = Common.deleteArrayElement(Inviters[inviter], userId);
            });
            delete Inviteds[userId];
        }
        if (_.has(Inviters, userId)) {
            delete Inviters[userId];
        }
        user.socket.leave('inGame');
        delete UserNames[getUserNameById(userId)];
        delete Users[userId];
    };
    var leaveGame = function(userId){
        var user = Users[userId];
        var gameId;
        if (user.isPlaying()) {
            gameId = user.getGameId();
            deleteGame(gameId);
        }
        if (user.isWatching()) {
            gameId = user.getGameId();
            var game = Games[gameId];
            game.removeWatcher(userId);
        }
    };
    var setUpUser = function(userName, socket) { // here
        var newUser = new User(userName, socket);
        UserNames[userName] = true;
        Users[newUser.userId] = newUser;
        return newUser;
    };
    return ({
        addInvitation: addInvitation,
        setUpUser: setUpUser,
        getUserById: getUserById,
        getLimitedUsersList: getLimitedUsersList,
        deleteUser: deleteUser,
        invitationExists: invitationExists,
        deleteInvitation: deleteInvitation,
        getUserNameById: getUserNameById,
        userNameExists: userNameExists,
        Inviteds: Inviteds,
        Games: Games,
        deleteGame: deleteGame,
        leaveGame: leaveGame
    });
};