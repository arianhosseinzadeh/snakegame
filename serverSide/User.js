var Common = require('../common/common');
var User = function(userName, skt) {
    var userId, status, gameId, socket;
    var _init = function(userName, skt) {
        status = 'free';
        userId = Common.randomGenerator(8);
        gameId = null;
        socket = skt;
        socket.join(userId);
        socket.join('inGame');// those who have username
    };
    var play = function(gId) {
        status = 'playing';
        socket.join(gId);
        gameId = gId;
    };
    var free = function() {
        socket.leave(gameId);
        gameId = null;
        status = 'free';
    };
    var watch = function(gId) {
        status = 'watching';
        gameId = gId;
        socket.join(gId);
    };
    var isFree = function() {
        return status === 'free';
    };
    var isPlaying = function() {
        return status == 'playing';
    };
    var isWatching = function() {
        return status == 'watching';
    };
    var getGameId = function() {
        return gameId;
    };
    _init(userName, skt);
    return ({
        userId: userId,
        isFree: isFree,
        isPlaying: isPlaying,
        isWatching: isWatching,
        free: free,
        play: play,
        watch: watch,
        getGameId: getGameId,
        userName: userName,
        socket: socket
    });
};
module.exports = User;