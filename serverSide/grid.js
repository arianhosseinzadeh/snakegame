var Snake = require('./snake');
var Common = require('../common/common');
var _ = require('lodash');
var Grid = function(userId1, userId2) {
    var grid = [];
    var snakes = {};
    var keyState = {};
    var gridDiff = {};
    var _init = function() {
        for (var yIndex = 0; yIndex < Common.columns; yIndex++) {
            grid.push([]);
            for (var xIndex = 0; xIndex < Common.rows; xIndex++) {
                grid[yIndex].push(Common.EMPTY);
            }
        }
        snakes[userId1] = new Snake(Common.RIGHT, 0, 0);
        snakes[userId2] = new Snake(Common.LEFT, Common.rows - 1, Common.columns - 1);
        _setPrize();
        _setCellContent(userId1, 0, 0);
        _setCellContent(userId2, Common.rows - 1, Common.columns - 1);
    };
    var updateGrid = function() {
        keyState[userId1] = keyState[userId1] || Common.KEY_RIGHT;
        keyState[userId2] = keyState[userId2] || Common.KEY_LEFT;
        var result = move(userId1, keyState[userId1]);
        if (result.status === 'continue') {
            return move(userId2, keyState[userId2]);
        } else {
            return result;
        }
    };
    var updateDirection = function(userId, ks) {
        if (ks === Common.KEY_DOWN || ks === Common.KEY_LEFT || ks === Common.KEY_RIGHT || ks === Common.KEY_UP) {
            keyState[userId] = ks;
        }
    };
    var move = function(userId, keyState) {
        var snake = snakes[userId];
        var newPosition = snake.move(keyState);
        var result;
        if (newPosition.x >= Common.columns || newPosition.x < 0 || newPosition.y >= Common.rows || newPosition.y < 0 ||
            (_getCellContent(newPosition.y, newPosition.x) !== Common.PRIZE &&
            _getCellContent(newPosition.y, newPosition.x) !== Common.EMPTY)) {
            var winnerId, loserId;
            if (userId === userId1) {
                winnerId = userId2;
                loserId = userId1;
            } else {
                winnerId = userId1;
                loserId = userId2;
            }
            result = {
                status: 'finish',
                winner: winnerId,
                loser: loserId
            }
        } else {
            if (_getCellContent(newPosition.y, newPosition.x) === Common.EMPTY) {
                var p = snake.remove();
                _setCellContent(Common.EMPTY, p.y, p.x);
            } else if (_getCellContent(newPosition.y, newPosition.x) === Common.PRIZE) {
                // it's a prize no need to remove the tail
                _setPrize();
            }
            snake.insert(newPosition.y, newPosition.x);
            _setCellContent(userId, newPosition.y, newPosition.x);
            result = {status: 'continue'};
        }
        return result;
    };
    var _getCellContent = function(y, x) {
        return grid[y][x];
    };
    var _setCellContent = function(value, y, x) {
        gridDiff[Common.convert2dTo1d(y , x, Common.columns)] = value;
        grid[y][x] = value;
    };
    var _setPrize = function() {
        var emptyCells = [];
        for (var y = 0; y < Common.rows; y++) {
            for (var x = 0; x < Common.columns; x++) {
                if (_getCellContent(y, x) === Common.EMPTY) {
                    emptyCells.push([y, x]);
                }
            }
        }
        var rand = Math.floor(Math.random() * emptyCells.length);
        var randX = emptyCells[rand][0];
        var randY = emptyCells[rand][1];
        _setCellContent(Common.PRIZE, randY, randX);
    };
    var getGridDiff = function() {
        return gridDiff;
    };
    var clearGridDiff = function() {
        gridDiff = {};
    };
    _init();
    return ({
        move: move,
        updateGrid: updateGrid,
        updateDirection: updateDirection,
        clearGridDiff: clearGridDiff,
        getGridDiff: getGridDiff
    });
};
module.exports = Grid;