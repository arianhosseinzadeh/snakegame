var _ = require('lodash');
var Game = require('./game');
var Common = require('../common/common');
var SocketsToUserId = {}; // socket.id to userId

module.exports = function(socket, io, gameStore) {
    socket.on('disconnect', function() {
        console.log('User with id : ' + socket.id + ' disconnected from the server');
        var userId = SocketsToUserId[socket.id];
        if (userId) {
            var user = gameStore.getUserById(userId);
            if (user.isPlaying()) {
                var gameId = user.getGameId();
                io.to(gameId).emit('playerLeft', {userName: user.userName});
            }
            gameStore.deleteUser(userId);
            io.to('inGame').emit('usersList', {users: gameStore.getLimitedUsersList()});
        }
    });
    socket.on('watch', function(data) {
        var gameId = data.groupId;
        var game = gameStore.Games[gameId];
        if (game) {
            var userId = SocketsToUserId[socket.id];
            game.addWatcher(userId);
            socket.emit('play', {
                rows: Common.rows,
                columns: Common.columns
            });
            io.to('inGame').emit('usersList', {users: gameStore.getLimitedUsersList()});
        }
    });
    socket.on('newUser', function(data) {
        if (!data.userName || data.userName.trim().length === 0) {
            socket.emit('message', {message: 'invalid user name'});
        } else if (gameStore.userNameExists(data.userName)) {
            socket.emit('message', {message: 'duplicate user name'});
        } else {
            var newUser = gameStore.setUpUser(data.userName, socket);
            SocketsToUserId[socket.id] = newUser.userId;
            socket.emit('gameStarted', {userId: newUser.userId});
            io.to('inGame').emit('usersList', {users: gameStore.getLimitedUsersList()});
        }
    });
    socket.on('newMove', function(data) {
        var userId = SocketsToUserId[socket.id];
        var user = gameStore.getUserById(userId);
        if (user && user.isPlaying()) {
            var gameId = user.getGameId();
            var game = gameStore.Games[gameId];
            if (game) {
                game.newDirection(userId, data.keyState);
            } else {
                user.free();
                socket.emit('userNamePage');
            }
        } else {
            socket.emit('userNamePage');
        }
    });
    socket.on('invitation', function(data) {
        var proposedOpponentId = data.userId;
        var userId = SocketsToUserId[socket.id];
        if (proposedOpponentId !== userId) {
            if (gameStore.getUserById(proposedOpponentId).isPlaying()) {
                socket.emit('busy', {userName: gameStore.getUserNameById(proposedOpponentId)});
            } else {
                gameStore.addInvitation(userId, proposedOpponentId);
                io.to(proposedOpponentId).emit('invited', {
                    userId: userId,
                    userName: gameStore.getUserNameById(userId)
                });
            }
        }
    });
    socket.on('acceptInvitation', function(data) {
            var userId = SocketsToUserId[socket.id];
            var acceptedUserId = data.userId;
            if (gameStore.invitationExists(acceptedUserId, userId)) {
                gameStore.Inviteds[userId].map(function(pendingInviterId) { // reject all OTHER users who invited userId
                    if (pendingInviterId !== acceptedUserId) {
                        io.to(pendingInviterId).emit('rejectInvitation', {userName: gameStore.getUserNameById(userId)});
                    }
                });
                gameStore.deleteInvitation(acceptedUserId, userId);
                gameStore.leaveGame(userId);
                gameStore.leaveGame(acceptedUserId);
                var game = new Game(userId, acceptedUserId, io, gameStore);
                var gameId = game.getGameId();
                gameStore.Games[gameId] = game;
                io.to(gameId).emit('play', {
                    rows: Common.rows,
                    columns: Common.columns
                });
                io.to('inGame').emit('usersList', {users: gameStore.getLimitedUsersList()});
            } else {
                socket.emit('message', {message: 'User either left or started playing with someone else'});
            }
        }
    );
    socket.on('rejectInvitation', function(data) {
        var userId = SocketsToUserId[socket.id];
        var rejectedUserId = data.userId;
        if (gameStore.invitationExists(rejectedUserId, userId)) {
            gameStore.deleteInvitation(rejectedUserId, userId);
            io.to(rejectedUserId).emit('rejectInvitation', {userName: gameStore.getUserNameById(userId)});
        } else {
            io.to(userId).emit('message', {message: 'User either left or started playing with someone else'});
        }
    });
};