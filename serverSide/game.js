var Grid = require('./grid');
var Common = require('../common/common');
var Game = function(userId1, userId2, io, gameStore) {

    var grid, gameId, players, watchers;

    var _init = function(userId1, userId2, io, gameStore) {
        players = [];
        watchers = [];
        gameId = generateGameId(userId1, userId2);
        grid = new Grid(userId1, userId2);
        players.push(userId1);
        players.push(userId2);
        var player1 = gameStore.getUserById(userId1);
        var player2 = gameStore.getUserById(userId2);
        player1.play(gameId);
        player2.play(gameId);
        io.to(gameId).emit('play', {
            rows: Common.rows,
            columns: Common.columns
        });
        setTimeout(periodicGridBroadcast, 1000 / Common.gameSpeed);
    };
    var isWatcher = function(watcherId) {
        Common.arrayContains(watchers, watcherId);
    };
    var isPlayer = function(userId) {
        return Common.arrayContains(players, userId);
    };
    var addWatcher = function(watcherId) {
        if (!isWatcher(watcherId)) {
            var watcher = gameStore.getUserById(watcherId);
            watcher.watch(gameId);
            watchers.push(watcherId);
        }
    };
    var removeWatcher = function(watcherId) {
        if (!isWatcher(watcherId)) {
            var watcher = gameStore.getUserById(watcherId);
            watcher.free();
            watchers = Common.deleteArrayElement(watchers, watcherId);
        }
    };
    var getPlayers = function() {
        return players;
    };
    var generateGameId = function(id1, id2) {
        return id1 + '' + id2;
    };
    var newDirection = function(userId, keyState) {
        grid.updateDirection(userId, keyState);
    };
    var getGameId = function() {
        return gameId;
    };
    var getGrid = function() {
        return grid;
    };
    var getNumberOfWatchers = function() {
        return watchers.length;
    };
    var getWatchers = function() {
        return watchers;
    };
    var periodicGridBroadcast = function() {
        var games = gameStore.Games;
        for (var gameId in games) {
            if (games.hasOwnProperty(gameId)) {
                var game = games[gameId];
                var grid = game.getGrid();
                var result = grid.updateGrid();
                if (result.status === 'continue') {
                    io.to(gameId).emit('newGrid', {newGrid: grid.getGridDiff()});
                    setTimeout(periodicGridBroadcast, 1000 / Common.gameSpeed)
                } else if (result.status === 'finish') {
                    io.to(gameId).emit('finish', {
                        winner: gameStore.getUserNameById(result.winner),
                        loser: gameStore.getUserNameById(result.loser)
                    });
                    gameStore.deleteGame(gameId);
                    io.to('inGame').emit('usersList', {users: gameStore.getLimitedUsersList()});
                }
                grid.clearGridDiff();
            }
        }
    };
    _init(userId1, userId2, io, gameStore);
    return ({
        getGameId: getGameId,
        getGrid: getGrid,
        newDirection: newDirection,
        isWatcher: isWatcher,
        removeWatcher: removeWatcher,
        addWatcher: addWatcher,
        getWatchers: getWatchers,
        getNumberOfWatchers: getNumberOfWatchers,
        isPlayer: isPlayer,
        getPlayers: getPlayers
    });
};
module.exports = Game;