var Common = require('../common/common');
var Snake = function(d, y ,x) {
    var direction = null;
    var queue = null;
    var _init = function(d, y, x) {
        direction = d;
        queue = [];
        insert(y, x);
    };
    var _getHead = function() {
        return queue[0];
    };
    var insert = function(y, x) {
        queue.unshift({x: x, y: y});
    };
    var remove = function() {
        return queue.pop();
    };
    var _setDirection = function(keyState) {
        if (keyState === Common.KEY_LEFT && direction !== Common.RIGHT) {
            direction = Common.LEFT;
        }
        if (keyState === Common.KEY_UP && direction !== Common.DOWN) {
            direction = Common.UP;
        }
        if (keyState === Common.KEY_RIGHT && direction !== Common.LEFT) {
            direction = Common.RIGHT;
        }
        if (keyState === Common.KEY_DOWN && direction !== Common.UP) {
            direction = Common.DOWN;
        }
    };
    var move = function(keyState) {
        _setDirection(keyState);
        var headX = _getHead().x;
        var headY = _getHead().y;
        switch (direction) {
            case Common.LEFT:
                headX--;
                break;
            case Common.UP:
                headY--;
                break;
            case Common.RIGHT:
                headX++;
                break;
            case Common.DOWN:
                headY++;
                break;
        }
        return {x: headX, y: headY};
    };
    _init(d, x, y);
    return ({
        insert: insert,
        remove: remove,
        move: move
    });
};
module.exports = Snake;
