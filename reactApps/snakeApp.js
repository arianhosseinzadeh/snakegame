var React = require('react');

var snake = React.createFactory(require('../components/snake'));
var initialState = JSON.parse(document.getElementById('initial-state').innerHTML);

React.render(
    snake(initialState),
    document.getElementById('react-app')
);