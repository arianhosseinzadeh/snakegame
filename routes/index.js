require('babel/register'); // JSX transpiler
var React = require('react'),
    Snake = React.createFactory(require('../components/snake.js')),
    router = require('express').Router();

router.get('/', function(req, res) {
    var initialState = {};
    var markup = React.renderToString(
        Snake(initialState)
    );
    res.render('snake', {
        markup: markup,
        state: JSON.stringify(initialState)
    });
});

module.exports = router;