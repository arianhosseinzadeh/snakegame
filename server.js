var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var routes = require('./routes/index');
var app = express();

// view engine setup
app.engine('handlebars', exphbs({defaultLayout: 'main', extname: '.handlebars'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');
//Note: Handlebars doesn't provide the necessary __express callback function to express app, therefore we use hbs
//which is compatible with express
//app.set('view engine', 'hbs') means : view engine of the app is hbs ,
// By default , hbs compiles all `.hbs` files actually it implies this line of code :
// app.engine('hbs', require('hbs').__express), as hbs provides the
//__express function
//app.set('view engine' , 'html')
//app.engine('html', require('hbs').__express)

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(logger('dev')); // the middleware provided by morgan to log in development environment
app.use(bodyParser.json()); // the middleware provided by bodyParser to parse json files for express
app.use(bodyParser.urlencoded({extended: false}));// the middleware provided by bodyParser to parse urlencoded files for
//express
app.use(cookieParser()); // a middleware which parses cookies
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
// defining the location for static files
//static resource are those files which do not change over time too frequently (from one request to the next one)
//and can be cached on both sides, we have to make sure to put version numbers in the name of these files
//anything that is data driven , is dynamic and is not static

app.use('/', routes); // mounting routes middleware in case we receive ANY (GET,POST,UPDATE, etc) for `/`
//app.get('/about`, about); note that this is the same as router.get(`...`). It's a bad practice to use app.get
//instead of router
//Generally it's better to use app.use and mount middleware (defined in other files , by router)

// catch 404 and forward to error handler
app.use(function(req, res, next) { // for all requests which were not caught by the previous two `use` cases
    var err = new Error('Not Found');
    err.status = 404;
    next(err); // we have to call next to call the next middleware in the stack
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var port = process.env.PORT || 9091;
var server = app.listen(port, function() {
    console.log('App is listening on port: ' + port);
});
var io = require('socket.io')(server);
var gameServer = require('./serverSide/gameServer');
var gameStore = new require('./serverSide/gameStore')();
io.on('connection', function(socket) {
    console.log('New client connected with session id ' + socket.id);
    new gameServer(socket, io, gameStore);
});

module.exports = app;
