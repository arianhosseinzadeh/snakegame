'use strict';

var React = require('react');

var MatchRow = React.createClass({

    displayName: '',

    propTypes: {
        socket: React.PropTypes.object,
        gameId: React.PropTypes.string
    },
    _watch: function() {
        this.socket.emit('watch', {groupId: this.props.gameId});
    },

    render: function() {
        this.socket = this.props.socket;
        var matches = this.props.matches;
        var numberOfWatchers = this.props.numberOfWatchers;
        return (<tr>
            <td>{matches[0]}</td>
            <td>VS.</td>
            <td>{matches[1]}</td>
            <td>{numberOfWatchers}</td>
            <td><img src="images/watch.ico" className="watchIcon" onClick={this._watch} alt="watch!"/></td>
        </tr>);
    }
});

module.exports = MatchRow;
