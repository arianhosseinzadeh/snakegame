'use strict';

var React = require('react');
var UserRow = require('./userRow.jsx');
var MatchRow = require('./matchRow.jsx');
var $ = require('jquery');
var UsersList = React.createClass({

    displayName: 'UsersList',

    propTypes: {
        socket: React.PropTypes.object,
        usersList: React.PropTypes.array,
        userId: React.PropTypes.string
    },
    render: function() {
        var userId = this.props.userId;
        var freeUsers = this.props.freeUsersList;
        var freeUsersList = [];
        if (freeUsers) {
            for (var freeUserId in freeUsers) {
                if (freeUsers.hasOwnProperty(freeUserId)) {
                    freeUsersList.push(<UserRow userId={freeUserId} ownUserId={userId}
                        userName = {freeUsers[freeUserId]}
                        socket={this.props.socket} key={'userRow' + freeUserId }
                    />);
                }
            }
        }
        var matches = this.props.matchesList;
        var matchesList = [];
        if (Object.keys(matches).length > 0) {
            for (var groupId in matches) {
                if (matches.hasOwnProperty(groupId)) {
                    matchesList.push(<MatchRow gameId={groupId} matches = {matches[groupId].players}
                        socket={this.props.socket} key={'matchRow' + groupId}
                        numberOfWatchers= {matches[groupId].numberOfWatchers}
                    />);
                }
            }
        } else {
            matchesList.push(<tr>
                <td>
                    <span className="no-matches">NO MATCHES YET !</span>
                </td>
            </tr>);
        }
        return (<div className="container users-list">
            <ul className="nav nav-tabs" id="myTab">
                <li className="active">
                    <a data-target="#players" data-toggle="tab">Players</a>
                </li>
                <li>
                    <a data-target="#matches" data-toggle="tab">Matches</a>
                </li>
            </ul>
            <div className="tab-content">
                <div id="players" className="tab-pane fade in active">
                    <div className="table-responsive">
                        <table className="table border-less">
                            <tbody key="tbody-players">
                                {freeUsersList}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="matches" className="tab-pane fade">
                    <div className="table-responsive">
                        <table className="table border-less">
                            <tbody key={'tbody-matches'}>
                                {matchesList}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>);
    }
});

module.exports = UsersList;
