'use strict';

var React = require('react');

var UserRow = React.createClass({

    displayName: 'UserRow',

    propTypes: {
        socket: React.PropTypes.object,
        userId: React.PropTypes.string
    },

    getDefaultProps: function() {
        return {};
    },

    _play: function() {
        this.socket.emit('invitation', {userId: this.props.userId});
    },

    render: function() {
        this.socket = this.props.socket;
        var result;
        if (this.props.ownUserId !== this.props.userId) {
            result = <tr>
                <td className="col-md-8">{this.props.userName}</td>
                <td className="col-md-4"><img src="images/play.ico" className="playIcon" onClick={this._play} alt="play!"/></td>
            </tr>;
        }else{
            result = <tr>
                <td>{this.props.userName}</td>
                <td></td>
            </tr>;
        }

        return result;
    }
});

module.exports = UserRow;
