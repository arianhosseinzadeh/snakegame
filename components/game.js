'use strict';
var React = require('react');
var Common = require('../common/common');
var $ = require('jquery');

var Game = React.createClass({

    displayName: 'Game',
    propTypes: function() {
        return ({
            grid: React.PropTypes.object,
            socket: React.PropTypes.object
        });
    },
    getInitialState: function() {
        var columns = Common.columns;
        var rows = Common.rows;
        var cellWidth = Common.boardWidth / rows;
        var cellHeight = Common.boardHeight / columns;
        var fillOpacity = 1;
        var cell;
        var fill;
        var initialGrid = [];
        for (var y = 0; y < columns; y++) {
            for (var x = 0; x < rows; x++) {
                var oneDimensionalCoordinate = Common.convert2dTo1d(y, x, columns);
                fill = 'grey';
                cell = (<rect width={cellWidth} height={cellHeight} x={x * cellWidth} y={y * cellHeight}
                    fill={fill} fillOpacity={fillOpacity} key={'cell' + oneDimensionalCoordinate}/>);
                initialGrid.push(cell);
            }
        }
        return ({
            grid: initialGrid
        });
    },
    _setNewState: function(index, newState) {
        this.setState(function(state) {
            state.grid.splice(index, 1, newState);
            return ({
                grid: state.grid
            });
        });
    },
    _updateGrid: function(toNew, grid) {
        this.setState(function(state) {
            var columns = Common.columns;
            var rows = Common.rows;
            var cellWidth = Common.boardWidth / rows;
            var cellHeight = Common.boardHeight / columns;
            var fill = '';
            for (var newValue in toNew) {
                if (toNew.hasOwnProperty(newValue)) {
                    var newCoordinate = Common.convert1dTo2d(newValue, columns);
                    var cellType = toNew[newValue];

                    var x = newCoordinate.x;
                    var y = newCoordinate.y;
                    if (cellType === this.props.userId) {
                        fill = 'green';
                        state.grid.splice(newValue, 1,  <rect width={cellWidth} height={cellHeight}
                            x={x * cellWidth} y={y * cellHeight} fill={fill} key={'cell' + newValue} />);
                    } else if (cellType === Common.EMPTY) {
                        fill = 'grey';
                        state.grid.splice(newValue, 1,  <rect width={cellWidth} height={cellHeight}
                            x={x * cellWidth} y={y * cellHeight} fill={fill} key={'cell' + newValue}/>);
                    } else if (cellType === Common.PRIZE) {
                        var imageString = "<image " +
                            "width= '" + cellWidth + "' height='" + cellHeight + "' x='" + (x * cellWidth) + "' y='"
                            + (y * cellHeight) + "' xlink:href='images/cookie.gif' />";
                        state.grid.splice(newValue, 1,  <svg dangerouslySetInnerHTML={{__html: imageString}} />);
                    } else { // the opponent
                        fill = 'red';
                        state.grid.splice(newValue, 1,  <rect width={cellWidth} height={cellHeight}
                            x={x * cellWidth} y={y * cellHeight} fill={fill} key={'cell' + newValue} />

                        );
                    }
                }
            }
            return ({
                grid: state.grid
            });
        });

    },
    componentDidMount: function() {
        this.socket = this.props.socket;

        $(document.body).on('keydown', function(event) {
            this.socket.emit('newMove', {keyState: event.keyCode});
        }.bind(this));

        this.socket.on('newGrid', function(data) {
            var newGrid = data.newGrid;
            var grid = this.state.grid;
            this._updateGrid(newGrid, grid);
        }.bind(this));
    },
    render: function() {
        return (
            <div className="game-board page-center">
                <svg width={Common.boardWidth} height={Common.boardHeight}>
                    {this.state.grid}
                </svg>
            </div>
        );
    }
});

module.exports = Game;
