'use strict';

var React = require('react');
var ioClient = require('socket.io-client');
var UsersList = require('./usersList');
var Game = require('./game');
var socket;
var LIST = 0, GAME = 1, USERNAME = 2;
var userId;
var Snake = React.createClass({

    displayName: 'Snake',

    propTypes: {},

    getDefaultProps: function() {
        return {};
    },
    getInitialState: function() {
        return ({
            environment: USERNAME,
            usersList: []
        });
    },
    componentWillUnmount: function() {
        this.socket.close();
    },
    componentDidMount: function() {
        socket = ioClient.connect();
        this.setState({
            connected: true
        });
        socket.on('usersList', function(data) {
            if (this.state.environment === GAME) {
                this.setState({
                    usersList: data.users
                });
            } else {
                this.setState({
                    environment: LIST,
                    usersList: data.users
                });
            }
        }.bind(this));
        socket.on('play', function(data) {
            this.setState({
                environment: GAME,
                rows: data.rows,
                columns: data.columns
            });
        }.bind(this));
        socket.on('playerLeft', function(data) {
            this.setState({
                environment: LIST
            });
            alert('user: ' + data.userName + ' left the game');
        }.bind(this));
        socket.on('gameStarted', function(data) {
            userId = data.userId;
        });
        socket.on('rejectInvitation', function(data) {
            if (this.state.environment === LIST) {
                alert(data.userName + ' was not interested in playing with you');
            }
        }.bind(this));

        socket.on('busy', function(data) {
            if (this.state.environment === LIST) {
                alert(data.userName + ' is already busy playing');
            }
        }.bind(this));
        socket.on('finish', function(data) {
            this.setState({
                environment: LIST
            });
            alert(data.winner + ' won the game');
        }.bind(this));
        socket.on('userNamePage', function(data) {
            this.setState({
                environment: USERNAME
            });
        }.bind(this));
        socket.on('invited', function(data) {
            if (this.state.environment === LIST) {
                var r = confirm("Are you interested in playing with " + data.userName);
                if (r === true) {
                    socket.emit('acceptInvitation', {userId: data.userId});
                } else {
                    socket.emit('rejectInvitation', {userId: data.userId});
                }
            }
        }.bind(this));
        socket.on('message', function(data) {
            var message = data.message;
            alert(message);
        });

    },
    _sendUserName: function() {
        var userName = this.refs.userNameInputBox.getDOMNode().value;
        socket.emit('newUser', {userName: userName});
    },
    render: function() {
        var result = null;
        if (this.state.connected) {
            if (this.state.environment === LIST) {
                result = <UsersList socket={socket} freeUsersList={this.state.usersList.freeUsers}
                    matchesList={this.state.usersList.matches} userId={userId}/>
            } else if (this.state.environment === GAME) { //game : true
                result = <Game socket={socket}
                    rows={this.state.rows} columns={this.state.columns} userId={userId}/>
            } else if (this.state.environment === USERNAME) {
                result = <div className="page-center login-page">
                    <div className="row">
                        <div className="row">
                            <img src="images/Snake-icon.png" />
                        </div>
                        <div className="row">
                            <form role="form">
                                <div className="form-group">
                                    <input type='text' ref="userNameInputBox" placeholder="Enter player name"
                                        className = "userNameInputBox"/>
                                </div>
                                <div className="button-center-wrapper">
                                    <input type="button" onClick={this._sendUserName}
                                        className = "btn btn-success text-center button-center" value = "ENTER" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            }
        } else {
            result = <div className="page-center">
                <div>
                    <h2 className="text-center">Connecting to the server</h2>
                    <div className="progress">
                        <div className="progress-bar progress-bar-striped active" style={{width: '100%'}}></div>
                    </div>
                </div>
            </div>;
        }
        // className : full-height is to place the div in the center
        return (<div className="full-height">
            {result}
        </div>)
    }
});

module.exports = Snake;
