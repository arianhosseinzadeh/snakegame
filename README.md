# Online Multiplayer Snake Game

An online Multi-player Snake Game, in which each game must have 2 players and arbitrary number of users watching the game. Each available online user can invite another user to play or watch a live match going on.

### Features:
- Each game has 2 players.
- Unlimited number of users can watch the game.
- It is a **live** application which means by use of Socket.io the game board is sent to both of the players and the watchers.  

Please check [Online Multiplayer Snake Game](http://ariansnake.herokuapp.com/) to see the demo of the app.

### Technologies:
In this project I used : 

* NodeJS
* ReactJS framework 
* Bootstrap
* Less.js
* Socket.io (which makes it a **live** application)

### Broadcasting the diffs !
Considering that the game server updates players and watchers of a game multiple times a second, sending the whole board of the game may lead to a lot of traffic. Therefore, I decided to only send the diff between each two subsequent boards (as majority of the cells remain same).

### Architecture
The app has two major parts : Server side & Client side. 
You can find the code related to the Client-side under `components` and the code related to the Server-side under `serverSide` (game engine and server) and `server.js` (express server)

### Run the code locally 
To run the code locally, first, clone the project on your local machine :
```sh
$ git clone git@bitbucket.org:arianhosseinzadeh/snakegame.git
```
Then, go to the directory of the project and install all the dependencies :
```sh
$ npm install
```
Build and run the code :
```sh
$ npm start
```
And finally check out the application in your browser [http://localhost:9091/](http://localhost:9091/)

- [How to install node ?](https://nodejs.org/download/)